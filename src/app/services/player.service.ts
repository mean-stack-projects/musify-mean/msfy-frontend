import { Injectable } from '@angular/core';
import {Howl, Howler} from 'howler';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class PlayerService{

    private playing = new Subject<boolean>();

    playingStatus$ = this.playing.asObservable();

    setPlaying(value: boolean){
        console.log(`Cambiando el valor por ${value}`)
        this.playing.next(value)
    }

    getPlaying(){
        return this.playingStatus$;
    }
}