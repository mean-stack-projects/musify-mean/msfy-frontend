import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';


@Injectable() //Permite injectar lo que sigue en otros lados
export class UserService{

	public url: string;
	public auth_url: string;
	public identity;
	public token;

	constructor(private _http: Http){
		this.url = GLOBAL.url;
		this.auth_url = GLOBAL.auth_url;
	}

	singup(user_to_login, gethash = null){

		if(gethash != null){
			user_to_login.gethash = gethash;
		}

		let json = JSON.stringify(user_to_login);
		let params = json;

		let headers = new Headers({
			'Content-type': 'application/json'});

		return this._http.post(this.auth_url +'signin'
			, params, {headers: headers})
			.map(res => res.json()); 
	}

	getIdentity(){
		let identity = JSON.parse(localStorage.getItem('identity'));

		if(identity != "undefined"){
			this.identity = identity;
		}
		else {
			this.identity = null;
		}

		return this.identity;
	}

	getToken(){
		let token = localStorage.getItem('token');

		if(token != "undefined"){
			this.token = token;
		}
		else {
			this.token = null;
		}

		return this.token;
	}

	register(user_to_register){
		let json = JSON.stringify(user_to_register);
		let params = json;

		let headers = new Headers({
			'Content-type': 'application/json'});

		return this._http.post(this.auth_url+'signup'
			, params, {headers: headers})
			.map(res => res.json()); 
	}

	updateUser(user_to_update){
		let json = JSON.stringify(user_to_update);
		let params = json;

		let headers = new Headers({
			'Content-type': 'application/json',
			'Authorization': this.getToken()});

		return this._http.put(this.url+'user/update/'+user_to_update._id
			, params, {headers: headers})
			.map(res => res.json()); 
	}

}