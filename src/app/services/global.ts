import {environment} from '../../environments/environment'

export var GLOBAL = {
	url: environment.APIEndpoint,
	auth_url: environment.APIAuth
}