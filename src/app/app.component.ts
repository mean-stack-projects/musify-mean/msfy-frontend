import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { GLOBAL } from './services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from './models/user';
import { GrowlModule }/*,Message}*/ from 'primeng/primeng';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({ //Acá cargo los servicios
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService, MessageService]
})
export class AppComponent implements OnInit{
  public title = 'MUSIFY';
  public user: User;
  public identity;
  public token; //Guardamos token en el front end
  public errorMessage;
  public user_register: User;
  public alertRegister;
  public url: string;
//  public _messageService: MessageService;
  public msgs: Message[] = [];
  public loading;

  constructor(private _userService: UserService, 
    private messageService: MessageService,
    private _route: ActivatedRoute,
    private _router: Router){
  	this.user = new User('','','','','','ROLE_USER','','');
    this.user_register = new User('','','','','','ROLE_USER','','');
    this.url = GLOBAL.url;
    this.loading = false;
  }

  ngOnInit(){
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  public onSubmit(){

    this.errorMessage = null;
    this.loading = true;

    document.getElementById("spinner-login").style.display = 'inline-block';

    //Conseguir los datos del usuario identificado
  	this._userService.singup(this.user)
  		.subscribe(
  			response => {
          
          this.identity = this._userService.getIdentity();
          this.token = this._userService.getToken();

  				let identity = response.user;

          this.identity = identity;

          if(!this.identity.id){
            console.log(identity)
            alert("El usuario no está correctamente identificado")
          } else{
            //Crear elemento en el local storage para la sesión
            localStorage.setItem('identity', JSON.stringify(identity));

            //Conseguir el token
            this._userService.singup(this.user, 'true')
            .subscribe(
              response => {
                let token = response.description.token;

                this.token = token;

                if(!token || this.token.length <= 0){
                  alert("El token no se generó");
                } else{
                  this.loading = false;
                  //Crear elemento en el local storage para tener el token disponible
                  localStorage.setItem('token', token);
                  this.user = new User('','','','','','ROLE_USER','','');
                  this.msgs = [];
                  this.msgs.push({severity:'success', summary:'Login exitoso', detail:'Credenciales correctas'});
                }
              },
              error => {
                
                var errorMessage = <any>error;
                if(errorMessage != null){

                  var body = JSON.parse(error._body);

                  this.errorMessage = body.message;

                  this.msgs = [];
                  this.msgs.push({severity:'error', summary:'Credenciales incorrectas', detail:'Combinación Email/Contraseña inválida'});
                  document.getElementById("spinner-login").style.display = 'none';
                }
              }
            );
          }
          document.getElementById("spinner-login").style.display = 'none';
  			},
  			error => {
          
  				var errorMessage = <any>error;
  				if(errorMessage != null){

            var body = JSON.parse(error._body);

            this.errorMessage = body.message;

            this.msgs = [];
            this.msgs.push({severity:'error', summary:'Credenciales incorrectas', detail:'Combinación Email/Contraseña inválida'});
            document.getElementById("spinner-login").style.display = 'none';
  				}
  			}
      );
  }

  logout(){

    localStorage.removeItem('identity');
    localStorage.removeItem('token');
    localStorage.removeItem('sound_song');
    localStorage.clear();
    this.identity = null;
    this.token = null;
    this._router.navigate(['/']); //Redirigir a home cuando cierro sesión
  }

  public onSubmitRegister(){

    this.alertRegister = '';

    if(this.user_register.name == null || this.user_register.name == '' ||
      this.user_register.surname == null || this.user_register.surname == '' ||
      this.user_register.email == null || this.user_register.email == '' ||
      this.user_register.password == null || this.user_register.password == ''){
      this.alertRegister = 'Falta completar algunos campos...';
      this.msgs = [];
      this.msgs.push({severity:'error', summary:'Error', detail:'Falta completar algunos campos...'});
      return;
    }

    this._userService.register(this.user_register).subscribe(
      response => {
        let user = response.user;

        this.user_register = user;

        if(!user.id){
          this.alertRegister = 'Error al registrar el usuario';
          this.msgs = [];
          this.msgs.push({severity:'error', summary:'Error', detail:'Error al registrar el usuario'});
        }
        else{
          this.alertRegister = 'Usuario registrado existosamente, ingrese con su email' + ' (' + this.user_register.email + ')';
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Éxito', detail:'Usuario registrado existosamente, ingrese con su email' + ' (' + this.user_register.email + ')'});
          this.user_register = new User('','','','','','ROLE_USER','','');
        }

      },
      error => {
        var body = JSON.parse(error._body);

        if(body.err.code == '11000'){
          this.alertRegister = 'El email ya fue registrado';
          this.msgs = [];
          this.msgs.push({severity:'error', summary:'Error', detail:'El email ya fue registrado'});
        }
        else{
          this.alertRegister = body.message;
          this.msgs = [];
          this.msgs.push({severity:'error', summary:'ERROR', detail:this.alertRegister});
        }
      });
  }

}
