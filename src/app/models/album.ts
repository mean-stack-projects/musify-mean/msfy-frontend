export class Album{
	
	constructor(
		public title: string,
		public description: string,
		public date_release: number,
		public image: string,
		public imageURL: string,
		public artist: string
	) {}
}