export class Song{
	
	constructor(
		public number: number,
		public name: string,
		public duration: string,
		public file: string,
		public fileURL: string,
		public album: string
	) {}
}