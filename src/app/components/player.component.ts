import {Component, OnInit} from '@angular/core';
import {GLOBAL} from '../services/global';
import {Song} from '../models/song';
import {Howl, Howler} from 'howler';
import { PlayerService } from '../services/player.service';

@Component({
	selector: 'player',
	templateUrl: '../views/player.html',
	providers: [PlayerService]
})

export class  PlayerComponent implements OnInit {
	
	public url: string;
	public song;
	public sound: Howl;
	public playing: boolean;
	public songLenght: string;
	public currentTime: string;

	constructor(private _playerService: PlayerService){
		this.url = GLOBAL.url;
		this.song = new Song(1, "", "", "", "", "");
	}

	ngOnInit(){

		var song = JSON.parse(localStorage.getItem('sound_song'));
		if(song){
			this.song = song;
			/*this.sound = new Howl({
				src: [song.fileURL],
				autoplay: false,
				onend: function(){
					console.log(`Canción \"${song.name}\" finalizada`)
				}
			})*/
		}
		else{
			this.song = new Song(1, "", "", "", "", "");
		}
	}

	backward(){
		console.log("backward presionado... pero no hace nada... todavia...")
	}

	play(){
		console.log("play presionado")
		this._playerService.getPlaying().subscribe(isPlaying => this.playing = isPlaying)
		console.log(this.playing)
	}

	pause(){
		console.log("pause presionado")
	}

	forward(){
		console.log("forward presionado... pero no hace nada... todavia...")
	}
}