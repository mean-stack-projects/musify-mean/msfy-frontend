import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { AlbumService } from '../services/album.service';
import { UploadService } from '../services/upload.service';
import { Artist } from '../models/artist';
import { Album } from '../models/album';
import { GrowlModule, MessagesModule } from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'album-edit',
	templateUrl: '../views/album-add.html',
	providers: [UserService, AlbumService, UploadService]
})

export class AlbumEditComponent implements OnInit {

	public titulo: string;
	public artist: Artist;
	public album: Album;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];
	public is_edit: boolean;
	public filesToUpload: Array<File>

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _albumService: AlbumService,
		private _uploadService: UploadService){

		this.titulo = 'Editar album';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.album = new Album('','',2017,'','','');
		this.is_edit = true;
	}

	ngOnInit(){
		//Llamar al método del api para sacar un artista en base a su id
		this.getAlbum();
	}

	getAlbum(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._albumService.getAlbum(this.token,id).subscribe(
				response =>{
					if(!response.album[0]){
						this._router.navigate(['/']);	
					}
					else{
						this.album = response.album[0];
						this.album.artist = response.album[0].artist_id
					}
					
				},
				error => {
					alert('Error al obtener los datos del álbum');
				}
			);
		});
	}

	onSubmit(){

		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._albumService.editAlbum(this.token, id, this.album).subscribe(
				response =>{
					if(!response){
						alert('No se editó el album')
					}
					else{
						alert(response.message);
						
						if(!this.filesToUpload){
							//Redirigir
							this._router.navigate(['/artista', this.album.artist]);
						}
						else{

							this._uploadService.makeFileRequest(this.url+'album/image/'+id,[], this.filesToUpload, this.token, 'image')
							.then(
								(result) => {
									this._router.navigate(['/artista', this.album.artist]);
								},
								(error) =>{
									alert("Error al subir la imagen")
								}
							);
						}
					}
				},
				error => {
					var errorMessage = JSON.parse(error._body);

					if(errorMessage != null){
						var body = JSON.parse(error._body);
						alert(errorMessage.message);
					}
				}
			);
		});
	}

	fileChangeEvent(fileInput: any){
		this.filesToUpload = <Array<File>> fileInput.target.files;
	}
}