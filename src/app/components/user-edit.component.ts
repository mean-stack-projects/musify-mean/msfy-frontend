import { UserService } from '../services/user.service';
import { User } from '../models/user';

import { GLOBAL } from '../services/global';
import { Component, OnInit } from '@angular/core';

import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'user-edit',
	templateUrl: '../views/user-edit.html',
	providers: [UserService]
})
export class UserEditComponent implements OnInit{
	public titulo: string;
	public user: User;
	public identity;
	public token;
	public alertMessage;
	public url: string;
	public msgs: Message[] = [];

	constructor(
		private _userService: UserService){

		this.titulo= 'Actualizar mis datos';
		

		//Local Storage
	    this.identity = this._userService.getIdentity();
	    this.token = this._userService.getToken();

	    this.user = this.identity;
	    this.url = GLOBAL.url;

	}

	ngOnInit(){
	    this.identity = this._userService.getIdentity();
	    this.token = this._userService.getToken();
	}

	onSubmit(){
		this._userService.updateUser(this.user).subscribe
		(
			response => {

				if(!response.user){
					this.alertMessage = 'El usuario no se ha actualizado';
				}
				else{
					//this.user = response.user;
					localStorage.setItem('identity', JSON.stringify(this.user));

					document.getElementById("identity_name").innerHTML = this.user.name;

					this.alertMessage = 'Datos actualizados correctamente';
					
					this.msgs = [];
                  	this.msgs.push({severity:'success', summary:'Actualización exitosa', detail:'El usuario fue actualizado correctamente'});

					if(!this.filesToUpload){
						//Redirección
					}
					else{
						this.makeFileRequest(this.url+'user/image/post/'+this.user._id, [], this.filesToUpload).then((result: any) => {
							this.user.image = result.image;
							this.user.imageURL = result.imageURL;
							
							localStorage.setItem('identity',JSON.stringify(this.user));

							//let image_path = this.url + 'user/image/get/' + this.user.image;
							document.getElementById('image-logged').setAttribute('src', result.imageURL);
						});
					}
				}
			},
			error =>{
				var errorMessage = <any>error;
                if(errorMessage != null){

                  var body = JSON.parse(error._body);

                  this.alertMessage = body.message;
                  this.msgs = [];
                  this.msgs.push({severity:'error', summary:'ERROR', detail:this.alertMessage});
			}
		});
	}

	public filesToUpload: Array<File>;

	fileChangeEvent(fileInput: any){
		this.filesToUpload = <Array<File>> fileInput.target.files; //Toma los archivos seleccionados en el input

	}

	//Petición ajax
	makeFileRequest(url: string, params: Array<string>, files: Array<File>){
		var token = this.token;

		return new Promise(function(resolve, reject){
			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();

			for(var i = 0; i < files.length; i++){
				formData.append('image', files[i], files[i].name);
			}

			xhr.onreadystatechange = function(){
				if(xhr.readyState == 4){
					if(xhr.status == 200){
						resolve(JSON.parse(xhr.response));
					}
					else{
						reject(xhr.response);
					}
				}
			}

			xhr.open('POST', url, true); 
			xhr.setRequestHeader('Authorization', token);
			xhr.send(formData);
		});
	}
}
