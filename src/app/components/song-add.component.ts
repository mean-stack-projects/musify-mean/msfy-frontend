import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';

import { SongService } from '../services/song.service';
import { Song } from '../models/song';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'song-add',
	templateUrl: '../views/song-add.html',
	providers: [UserService, SongService]
})

export class SongAddComponent implements OnInit {

	public titulo: string;
	public song: Song;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _songService: SongService){

		this.titulo = 'Subir canción';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.song = new Song(1,'','','','','');
	}

	ngOnInit(){}
	
	onSubmit(){
		
		this._route.params.forEach((params: Params) =>{
			let album_id = params['album'];
			this.song.album = album_id;

			this._songService.addSong(this.token, this.song)
				.subscribe(
				response =>{
					if(!response.inserted_id){
						alert('No se guardó la canción')
					}
					else{
						alert('Canción creada exitosamente');

						this._router.navigate(['/editar-tema', response.inserted_id]);
					}
				},
				error => {
					var errorMessage = <any>error;

					if(errorMessage != null){
						var body = JSON.parse(error._body);
						alert(body.message);
					}
				}
			);
		});
		
	}
}