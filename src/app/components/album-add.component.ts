import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { AlbumService } from '../services/album.service';
import { Artist } from '../models/artist';
import { Album } from '../models/album';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'album-add',
	templateUrl: '../views/album-add.html',
	providers: [UserService, ArtistService, AlbumService]
})

export class AlbumAddComponent implements OnInit {

	public titulo: string;
	public artist: Artist;
	public album: Album;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];
	public is_edit;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService,
		private _albumService: AlbumService){

		this.titulo = 'Crear nuevo álbum';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.album = new Album('','',2017,'','','');
		this.is_edit = false;
	}

	ngOnInit(){
		
	}

	onSubmit(){

		this._route.params.forEach((params: Params) =>{
			let artist_id = params['id'];
			this.album.artist = artist_id;

			this._albumService.addAlbum(this.token, this.album).subscribe(
				response =>{
					if(!response.inserted_id){
						alert('No se guardó el album')
					}
					else{
						alert('Album creado exitosamente');
						this.album = response.album;

						this._router.navigate(['/editar-album', response.inserted_id]);
					}
				},
				error => {
					var errorMessage = <any>error;

					if(errorMessage != null){
						var body = JSON.parse(error._body);
						alert(body.message);
					}
				}
			);
		});
	}
}