import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { AlbumService } from '../services/album.service';
import { Artist } from '../models/artist';
import { Album } from '../models/album';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'artist-detail',
	templateUrl: '../views/artist-detail.html',
	providers: [UserService, ArtistService, AlbumService]
})

export class ArtistDetailComponent implements OnInit {

	public titulo: string;
	public artist: Artist;
	public albums: Album[];
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService,
		private _albumService: AlbumService){

		//this.titulo = 'Detalles del artista';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
	}

	ngOnInit(){
		this.getArtist();
		//Llamar al método del api para sacar un artista en base a su id
	}

	getArtist(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id']; //Agarro el id que llega por la url
		
			this._artistService.getArtist(this.token, id).subscribe(
				response => {

					if(!response.artists){
						this._router.navigate(['/']);
					}
					else{
						this.artist = response.artists[0];
						//Obtener los albums del artista
						this._albumService.getAlbums(this.token, id)
						.subscribe(
							response=>{

								if(!response.album){
									alert('no hay albums');
								}
								else{
									this.albums = response.album;
								}
							},
							error =>{
								var errorMessage = <any>error;
								if(errorMessage != null){
									var body = JSON.parse(error._body);
									alert("Artista sin albums");
									//this._router.navigate(['/artistas/1']);
								}
							}
						);
					}
				},
				error => {
					var body = JSON.parse(error._body);
                  	this.msgs = [];
                  	this.msgs.push({severity:'error', summary:'ERROR', detail:body});
				}
			)
		});
	}

	public confirmado;

	onDeleteConfirm(id){
		this.confirmado = id;
	}

	onCancelAlbum(){
		this.confirmado = null;
	}

	onDeleteAlbum(id){
		this._albumService.deleteAlbum(this.token,id).subscribe(
			response =>{
				if(!response.info_reg.deleted_id){
					alert('No se borró el album, ni idea por qué');
				}
				else{
					alert('Álbum eliminado exitosamente');
				}

				this.getArtist();
			},
			error => {
				alert('Error al intentar eliminar el álbum');
			}
		);
	}

}