import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { Artist } from '../models/artist';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'artist-add',
	templateUrl: '../views/artist-add.html',
	providers: [UserService, ArtistService]
})

export class ArtistAddComponent implements OnInit {

	public titulo: string;
	public artist: Artist;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService){

		this.titulo = 'Crear Nuevo artista';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.artist = new Artist('','','','');
	}

	ngOnInit(){
	}

	onSubmit(){

		this._artistService.addArtist(this.token, this.artist).subscribe(
			response  => {
				if(response.exists){
					alert(response.message)
					return
				}

				alert(response.description.message)

				if(response.code == 200){
					this._router.navigate(['/editar-artista', response.description.id] );
				}
			},
			error =>{
                this.msgs = [];
				this.msgs.push({severity:'error', summary:'ERROR', detail: error.message});
				alert(error.message)
			});
	}
}