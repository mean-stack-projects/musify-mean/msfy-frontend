import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { UploadService } from '../services/upload.service';
import { Artist } from '../models/artist';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'artist-edit',
	templateUrl: '../views/artist-add.html',
	providers: [UserService, ArtistService, UploadService]
})

export class ArtistEditComponent implements OnInit {

	public titulo: string;
	public artist: Artist;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];
	public is_edit;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService,
		private _uploadService: UploadService){

		this.titulo = 'Actualizar artista';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.artist = new Artist('','','','');
		this.is_edit = true;
	}

	ngOnInit(){
		this.getArtist();
		//Llamar al método del api para sacar un artista en base a su id
	}

	getArtist(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id']; //Agarro el id que llega por la url
		
			this._artistService.getArtist(this.token, id).subscribe(
				response => {

					if(!response.artists[0]){
						this._router.navigate(['/']);
					}
					else{
						this.artist = response.artists[0];
					}
				},
				error => {
					var body = JSON.parse(error._body);
                  	this.msgs = [];
                  	this.msgs.push({severity:'error', summary:'ERROR', detail:body.message});
				}
			)
		});
	}

	onSubmit(){

		this._route.params.forEach((params: Params) => {
		let id = params['id'];


		this._artistService.editArtist(this.token, id,this.artist).subscribe(
			response  => {

				if(!response.id){
					this.msgs = [];
                  	this.msgs.push({severity:'error', summary:'ERROR', detail:'Servidor no respondió'});
				}
				else{

					if(!this.filesToUpload){
						this._router.navigate(['/artista', response.id]);
					}
					else{

					}
					//Subir imagen del artista
					this._uploadService.makeFileRequest(this.url+'upload-image-artist/'+id,[], this.filesToUpload, this.token, 'image')
					.then(
						(result) => {
							this._router.navigate(['/artista', response.artist._id]);
						},
						(error) =>{

							this.msgs = [];
                  			this.msgs.push({severity:'error', summary:'ERROR', detail:'Error al subir la imagen del usuario'});
						}
					);

					//this.artist = response.artist;
					//this._router.navigate(['/editar-artista'], response.artist._id);
					this.msgs = [];
                  	this.msgs.push({severity:'success', summary:'Éxito', detail:'Artista actualizado correctamente'});	
					//this.artist = new Artist('','','');
				}
			},
			error =>{
                  var body = JSON.parse(error._body);

                  this.msgs = [];
                  this.msgs.push({severity:'error', summary:'ERROR', detail: body.message});
			});
		})
	}

	public filesToUpload: Array<File>;

	fileChangeEvent(fileInput: any){
		this.filesToUpload = <Array<File>> fileInput.target.files;
	}
}