import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';

import { UploadService } from '../services/upload.service';

import { SongService } from '../services/song.service';
import { Song } from '../models/song';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'song-edit',
	templateUrl: '../views/song-add.html',
	providers: [UserService, SongService, UploadService]
})

export class SongEditComponent implements OnInit {

	public titulo: string;
	public song: Song;
	public identity;
	public token;
	public url: string;
	public msgs: Message[] = [];
	public is_edit;
	public filesToUpload;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _songService: SongService,
		private _uploadService: UploadService){

		this.titulo = 'Editar canción';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.song = new Song(1,'','','','','');
		this.is_edit = true;
	}

	ngOnInit(){
	
		// Sacar cancion a editar
		this.getSong();
	}

	getSong(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._songService.getSong(this.token, id)
				.subscribe(
					response =>{
						if(!response.song[0]){
							alert('No se pudo recuperar la canción');
							this._router.navigate(['/']);
						}
						else{
							this.song = response.song[0];
							this.song.album = response.song[0].album._id;
						}
					},
					error => {
						var errorMessage = <any>error;

						if(errorMessage != null){
							var body = JSON.parse(error._body);
							alert(body.message);
						}
					}
				);
		});
	}

	onSubmit(){
		
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];

			this._songService.editSong(this.token, id, this.song)
				.subscribe(
				response =>{
					if(!response.description.message){
						alert('No se editó la canción')
					}
					else{
						alert(response.description.message);

						if(!this.filesToUpload){
							this._router.navigate(['/album', this.song.album]);
						}
						else{
						//Subir fichero de audio
							this._uploadService.makeFileRequest(this.url+'song/file/'+id, [], this.filesToUpload, this.token, 'file')
								.then(
									(result) => {
										this._router.navigate(['/album', this.song.album]);
									},
									(error) =>{
										alert("Error al subir el fichero de audio");
									}
								);
						}

						//this._router.navigate(['/editar-album', response.albumStored._id]);
					}
				},
				error => {
					var errorMessage = <any>error;

					if(errorMessage != null){
						var body = JSON.parse(error._body);
						alert(body);
					}
				}
			);
		});
		
	}

	fileChangeEvent(fileInput: any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
	}
}