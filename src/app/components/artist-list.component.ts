import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { Artist } from '../models/artist';

import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';

@Component({
	selector: 'artist-list',
	templateUrl: '../views/artist-list.html',
	providers: [UserService, ArtistService]
})

export class ArtistListComponent implements OnInit {

	public titulo: string;
	public artists: Artist[];
	public identity;
	public token;
	public url: string;
	public next_page;
	public prev_page;
	public msgs: Message[] = [];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService){

		this.titulo = 'Artistas';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.next_page = 1;
		this.prev_page = 1;
	}

	ngOnInit(){

		this.getArtists();
		//Conseguir el listado de artistas
	}

	getArtists(){
		this.artists = null;
		this._route.params.forEach((params: Params) =>{
			let page = +params['page']; //El + convierte a número
			
			if(!page){
				page = 1;
			}
			else{
				this.next_page = page + 1;
				this.prev_page = page - 1;

				if(this.prev_page == 0){
					this.prev_page = 1;
				}
			}

			this._artistService.getArtists(this.token, page).subscribe(

				response =>{
					if(!response.artists){
						this.msgs = [];
						this.msgs.push({severity:'info', summary:'Atención', detail:'No se encontraron artistas'});
					}
					else{
						this.artists = response.artists;
					}

				},
				error => {
					var errorMessage = <any>error;
					if(errorMessage != null){

						var body = JSON.parse(error._body);

						this.msgs = [];
						this.msgs.push({severity:'error', summary:'ERROR', detail:"No se encontraron artistas"});
					}
				}
				);
		});
	}

	public confirmado;
	
	onDeleteConfirm(id){
		this.confirmado = id;
	}

	onCancelArtist(){
		this.confirmado = null;
	}

	onDeleteArtist(id){
		this._artistService.deleteArtist(this.token, id).subscribe(
			response => {
				if(!response.artist){
					this.msgs = [];
					this.msgs.push({severity:'error', summary:'ERROR', detail:'Error en el servidor'});
				}

				this.msgs = [];
				this.msgs.push({severity:'success', summary:'Éxito', detail:'Artista eliminado correctamente'});

				this.getArtists();
			},	
			error => {

				var errorMessage = <any>error;

				if(errorMessage != null){
					var body = JSON.parse(error._body);

					this.msgs = [];
					this.msgs.push({severity:'error', summary:'ERROR', detail:body});
				}
			}
		);
	}
}