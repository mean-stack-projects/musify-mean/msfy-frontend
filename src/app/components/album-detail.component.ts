import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { AlbumService } from '../services/album.service';
import { SongService } from '../services/song.service';
import { Album } from '../models/album';
import { Song } from '../models/song';
import { GrowlModule, MessagesModule }/*,Message}*/ from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import {Howl, Howler} from 'howler';
import { PlayerService } from '../services/player.service';

@Component({
	selector: 'album-detail',
	templateUrl: '../views/album-detail.html',
	providers: [UserService, AlbumService, SongService, PlayerService]
})

export class AlbumDetailComponent implements OnInit {

	public album: Album;
	public identity;
	public songs: Song[];
	public token;
	public url: string;
	public msgs: Message[] = [];
	public sound: Howl;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _albumService: AlbumService,
		private _songService: SongService,
		private _playerService: PlayerService){

		//this.titulo = 'Detalles del artista';
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
	}

	ngOnInit(){
		//Sacar album de la bdd
		this.getAlbum()
	}

	getAlbum(){
		this.songs = null
	
		this._route.params.forEach((params: Params) =>{
			let id = params['id']; //Agarro el id que llega por la url
		
			//Obtener las canciones asosiadas al album tambien
			this._albumService.getAlbum(this.token, id).subscribe(
				response => {
					if(!response.album[0]){
						this._router.navigate(['/']);
					}
					else{
						this.album = response.album[0];

						this._songService.getSongs(this.token, id)
						.subscribe(
							response => {
								if(!response.song){
									alert("El album no tiene canciones");
									//this._router.navigate(['/']);
								}
								
								this.songs = response.song ? response.song : null;
							},
							error => {

								var body = JSON.parse(error._body)
								alert(body.message)
							}
						)
					}
				},
				error => {
					var body = JSON.parse(error._body);
                  	this.msgs = [];
                  	this.msgs.push({severity:'error', summary:'ERROR', detail:body});
				}
			)
		});
	}

	public confirmado;

	onDeleteConfirm(id){
		this.confirmado = id;
	}

	onCancelSong(){
		this.confirmado = null;
	}

	onDeleteSong(id){
		this._songService.deleteSong(this.token, id)
		.subscribe(
			response =>{
				if(!response.message){
					alert("No se eliminó la canción (Error de servidor)");
					return
				}

				alert(response.message);

				this.getAlbum();
			},
			error => {
				var errorMessage = <any>error;
				if(errorMessage != null){
					alert("Ocurrió un error");
				}
			}
		);
	}

	startPlayer(song){

		if(this.sound != null){
			this.sound.stop();
			this.sound.unload();
			this.sound = null;
		}

		let song_player = JSON.stringify(song);
		let file_path = song.fileURL;
		let image_path = song.album.imageURL;

		localStorage.setItem('sound_song', song_player);

		this.sound = new Howl({
			src: [song.fileURL],
			autoplay: false,
			loop: false,
			volume: 0.5,
			onend: function() {
				console.log('Finalizó la canción!');
			}
		})

		this._playerService.setPlaying(true)

		this.sound.play()
		

	
		/*document.getElementById("mp3-source").setAttribute('src', file_path);

		(document.getElementById('player') as any).load(); //Fuerza a que el dato sea tipo any

		(document.getElementById('player') as any).play();

		document.getElementById('play-song-file').innerHTML = song.name;

		document.getElementById('play-song-artist').innerHTML = song.album.artist.name;

		document.getElementById('play-image-album').setAttribute("src",image_path);*/
	}
}